package com.example.exeriseviewmodellivedatadatabinding.utils

enum class Popularity {
    NORMAL,
    POPULAR,
    STAR
}
