package com.example.exeriseviewmodellivedatadatabinding.binding

import android.content.Context
import android.content.res.ColorStateList
import android.view.View
import android.widget.ProgressBar
import androidx.core.content.ContextCompat
import androidx.databinding.BindingAdapter
import com.example.exeriseviewmodellivedatadatabinding.R
import com.example.exeriseviewmodellivedatadatabinding.utils.Popularity

@BindingAdapter("app:hideIfZero")
fun hideIfZero(view: View, number: Int) {
    view.visibility = if (number == 0) View.GONE else View.VISIBLE
}

@BindingAdapter("app:progressTint")
fun tinPopularity(view: ProgressBar, popularity: Popularity) {
    val color = getAssociatedColor(popularity, view.context)
    view.progressBackgroundTintList = ColorStateList.valueOf(color)
}

private fun getAssociatedColor(popularity: Popularity, context: Context): Int {
    return when (popularity) {
        Popularity.NORMAL -> context.theme.obtainStyledAttributes(
            intArrayOf(android.R.attr.colorForeground)
        ).getColor(0, 0x000000)
        Popularity.POPULAR -> ContextCompat.getColor(context, R.color.popular)
        Popularity.STAR -> ContextCompat.getColor(context, R.color.star)
    }
}
