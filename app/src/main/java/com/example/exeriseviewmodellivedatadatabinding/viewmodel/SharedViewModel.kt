package com.example.exeriseviewmodellivedatadatabinding.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.exeriseviewmodellivedatadatabinding.model.User

class SharedViewModel : ViewModel() {
    val selected = MutableLiveData<User>()

    val selectedUser = User("", "");

    val isEnable = MutableLiveData(true)

    fun select() {
        this.selected.value = selectedUser
    }
}
