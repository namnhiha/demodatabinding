package com.example.exeriseviewmodellivedatadatabinding.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.example.exeriseviewmodellivedatadatabinding.utils.Popularity

class SimpleViewModel : ViewModel() {
    private var mFirstName = MutableLiveData("Phuong")
    private var mLastName = MutableLiveData("Nam")
    private var mLike = MutableLiveData(0)

    val firtName: LiveData<String> = mFirstName
    val lastName: LiveData<String> = mLastName
    val like: LiveData<Int> = mLike

    val popularity: LiveData<Popularity> = Transformations.map(mLike) {
        when {
            it > 9 -> Popularity.STAR
            it > 4 -> Popularity.POPULAR
            else -> Popularity.NORMAL
        }
    }

    fun onLike() {
        mLike.value = (mLike.value ?: 0) + 1
    }
}
