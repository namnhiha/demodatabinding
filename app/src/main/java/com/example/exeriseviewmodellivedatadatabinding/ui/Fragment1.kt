package com.example.exeriseviewmodellivedatadatabinding.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.example.exeriseviewmodellivedatadatabinding.databinding.Fragment1Binding
import com.example.exeriseviewmodellivedatadatabinding.viewmodel.SharedViewModel

class Fragment1 : Fragment() {
    private val viewModel by lazy {
        ViewModelProviders.of(this).get(SharedViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val listItemBinding: Fragment1Binding =
            Fragment1Binding.inflate(inflater, container, false)
        val view = listItemBinding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }
}
